const zlib = require('zlib');
const Configurapi = require('configurapi');

async function decompress(data)
{
    return new Promise((resolve, reject) => 
    {
        zlib.gunzip(data, (err, res) => 
        {
            if (err) return reject(err);
            
            let result = JSON.parse(res.toString('utf8'));
            
            resolve(result);
        });
    });
}

module.exports = {
    toResponse: function(response)
    {
        return {
            statusCode: response.statusCode,
            headers: response.headers,
            body: JSON.stringify(response.body)
        };
    },

    toRequest: async function(awsEvent)
    {
        let payload = new Buffer(awsEvent.awslogs.data, 'base64');
        let eventData = await decompress(payload);

        let request = new Configurapi.Request();

        request.name = process.env.event_name || ''
        request.method = '';
        request.headers = {};
        
        request.payload = eventData.logEvents;
        request.query = {};
        request.path = '';
        request.pathAndQuery = '';

        try
        {
            if(request.payload)
            {
                request.payload = JSON.parse(request.payload);  
            }
        }
        catch(e){} 

        return request;
    }
};
