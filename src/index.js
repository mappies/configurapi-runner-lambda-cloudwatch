const Configurapi = require('configurapi');
const oneliner = require('one-liner');
const Adapter = require('./LambdaAdapter');

///////////////////////////////////
// Utility functions
///////////////////////////////////
function log(prefix, str)
{
    let time = new Date().toISOString();
    console.debug(`${time} ${prefix}: ${oneliner(str)}`); //Use console.debug to exclude "$date $awsRequestId" prefix in CloudWatch logs
}


///////////////////////////////////
// Start service
///////////////////////////////////

const config = Configurapi.Config.load('./config.yaml');
const service = new Configurapi.Service(config);

service.on("trace", (s) => log('Trace', s));
service.on("debug", (s) => log('Debug', s));
service.on("error", (s) => log('Error', s));

///////////////////////////////////
// Entry point
///////////////////////////////////
exports.handler = async (awsEvent, context, callback) => 
{
    try
    {

        let event = new Configurapi.Event(await Adapter.toRequest(awsEvent, context));
        event.id = context.awsRequestId;
     
        
        let logWithEvent = (logLevel, event, message) => log(logLevel, `${event ? event.id : ''} - ${event ? event.correlationId : ''} - ${typeof message === 'string' ? message : JSON.stringify(message)}`);

        console.log = (message) => logWithEvent('Trace', event, message);
        console.error = (message) => logWithEvent('Error', event, message);
        console.warn = console.log;

        await service.process(event);
        
        callback(undefined, Adapter.toResponse(event.response))
    }
    catch(error)
    {
        let response = new Configurapi.ErrorResponse(error, -1);
        
        callback(JSON.stringify(response));
    }
};